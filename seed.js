const db = require('./app/config/db.config')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs') 
exports.seed = () => {
    db.sequelize.sync({force:true}).then(()=> {
        db.role.create({
            role:'super admin'
        }).then((role) => {
            //console.log(role)
            db.user.create({
                name: 'admin',
                email: 'admin@itasset.com',
                password: bcrypt.hashSync('admin', 10),
                roleId: 1
            }).then(result => {
                //console.log(result)
                db.permission.create({
                    roleId: 1,
                    resource: '*',
                    action: '*',
                    attributes: '*',
                    condition: '*'
                })
            }).catch(err => {
                //console.log(err)
            })
        }).catch(err => {
            //console.log(err)
        })
    }).catch(err => {
        //console.log(err)
    })
}