const express = require("express");
const app = express();
const bodyParser = require("body-parser");
require("dotenv").config();
const cors = require("cors");
app.use(cors());
app.use(bodyParser.json());
const { seed } = require("./seed");

const db = require("./app/config/db.config");

const server = app.listen(2020, function () {
  const host = server.address().address;
  const port = server.address().port;
  //console.log("connected");
});

app.get("/", function (req, res) {
  res.json({
    success: true,
  });
});

db.sequelize.sync({ alter: true }).then(async () => {
  const data = await db.user.findAndCountAll();
  if (data.count == 0) {
    {
      seed();
    }
  }
});

require("./app/router/middleware")(app);
require("./app/router/user.router")(app);
require("./app/router/asset.router")(app);
require("./app/router/brand.router")(app);
require("./app/router/category.router")(app);
require("./app/router/company.router")(app);
require("./app/router/department.router")(app);
require("./app/router/employee.router")(app);
require("./app/router/event.router")(app);
require("./app/router/instance.router")(app);
require("./app/router/location.router")(app);
//require('./app/router/message.router')(app)
require("./app/router/model.router")(app);
require("./app/router/permission.router")(app);
require("./app/router/purchase.router")(app);
require("./app/router/region.router")(app);
require("./app/router/role.router")(app);
require("./app/router/site.router")(app);
require("./app/router/specification.router")(app);
require("./app/router/specificationValue.router")(app);
require("./app/router/status.router")(app);
