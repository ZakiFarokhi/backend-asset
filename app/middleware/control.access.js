const AccessControl = require("role-acl");
const { permission } = require("../config/db.config");
const { response } = require("./response");
exports.access = async (role, resource, action) => {
  try {
    const Permission = await permission.findOne({
      where: {
        roleId: role,
        resource: role == 1 ? "*" : resource,
        action: role == 1 ? "*" : action,
      },
    });
    if (Permission) {
      return Permission;
    } else {
      return null;
    }
  } catch (error) {
    return null;
  }
};
exports.bridges = async (req, res, next) => {
  try {
    const Access = await this.access(
      res.locals.roleId,
      req.params.table,
      req.params.action
    );
    if (!Access) {
      response(res, false, "you are not authorized");
    }
    res.locals.access = JSON.stringify(Access);
    next()
  } catch (error) {
    response(res, false, "cannot connect db", error);
  }
};
