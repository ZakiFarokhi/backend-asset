module.exports = (sequelize, Sequelize) => {
  return sequelize.define("log", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    query: {
      type: Sequelize.STRING(1000),
    },
    value: {
      type: Sequelize.STRING(1000),
	},
	userId:{
		type: Sequelize.INTEGER
	}
  });
};
