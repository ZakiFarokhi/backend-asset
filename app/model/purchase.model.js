module.exports = (sequelize, Sequelize) => {
    return sequelize.define('purchase', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        order_number: {
            type: Sequelize.STRING(100)
        },
        date: {
            type: Sequelize.DATEONLY
        },
        from: {
            type: Sequelize.STRING(100)
        },
        document: {
            type: Sequelize.STRING
        }
    });
}