module.exports = (sequelize, Sequelize) => {
    return sequelize.define('employee', {
        id: {
            type: Sequelize.STRING(20),
            primaryKey: true,
           
        }, name: {
            type: Sequelize.STRING(50)
        }, title: {
            type: Sequelize.STRING(20)
        }, phone: {
            type: Sequelize.INTEGER.UNSIGNED.ZEROFILL
        }, email: {
            type: Sequelize.STRING(40),
        
            validate : {
				isEmail:true
			}
        }, password: {
            type: Sequelize.STRING
        }, isActive: {
            type: Sequelize.INTEGER
        }, status: {
            type: Sequelize.STRING(50)
        }
    })
}