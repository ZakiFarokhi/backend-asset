module.exports = (sequelize, Sequelize) => {
    return sequelize.define('event', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING(50)
        },
        description: {
            type: Sequelize.STRING
        },
        date: {
            type: Sequelize.DATEONLY
        },
        document: {
            type: Sequelize.STRING
        }
    })
}