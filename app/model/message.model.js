module.exports = (sequelize, Sequelize) => {
    return sequelize.define('message', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        from: {
            type: Sequelize.STRING(50)
        },
        to: {
            type: Sequelize.STRING
        },
        event: {
            type: Sequelize.INTEGER
        },
        asset: {
            type: Sequelize.INTEGER
        },
        isActive: {
            type: Sequelize.BOOLEAN
        }
    })
}