module.exports = (sequelize, Sequelize) => {
  return sequelize.define("permissions", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    resource: {
      type: Sequelize.STRING(50),
    },
    action: {
      type: Sequelize.STRING(10),
    },
    attributes: {
      type: Sequelize.STRING,   
    },
    condition: {
      type: Sequelize.STRING,
    },
  });
};
