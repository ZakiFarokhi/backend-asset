module.exports = (sequelize, Sequelize) => {
    return sequelize.define('specificationValue', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        value: {
            type: Sequelize.STRING
        }
    })
}