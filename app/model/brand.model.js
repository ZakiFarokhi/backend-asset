module.exports = (sequelize, Sequelize) => {
    return sequelize.define('brand', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING(50),
            
        }
    })
}