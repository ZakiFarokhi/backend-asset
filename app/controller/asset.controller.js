const db = require("../config/db.config");
const data = db.asset;
const { response } = require("../middleware/response");
const {logging} = require('../controller/log.controller')
const {employee,valueToAsset,category, brand, model, site, location, department, instance, company, region, status, purchase} = require('../config/db.config')
const multer = require('multer')
const readXlsxFile = require('read-excel-file')
exports.create = async (req, res) => {
  try {
    const {
      tag_id,
      description,
      categoryId,
      brandId,
      modelId,
      serial_no,
      siteId,
      locationId,
      departmentId,
      lastOpname,
      statusId,
      instanceId,
      companyId,
      regionId,
      isActive,
      cost,
      purchaseId,
      specificationValues
    } = req.body;
    const Create = await data.create({
      tag_id,
      description,
      categoryId,
      brandId,
      modelId,
      serial_no,
      siteId,
      locationId,
      departmentId,
      lastOpname,
      statusId,
      instanceId,
      companyId,
      regionId,
      isActive,
      cost,
      purchaseId,
      specificationValues
    }, {logging:(...msg) => {logging(msg, req.body.userId); console.log(msg, req.body.userId)}});
    if (!Create) {
      response(res, false, "cannot create data", null);
    } else {
      let array = []
      for (const value of specificationValues){
        console.log(value)
        array.push(value)
      }
      const CreateJoin = await Create.setSpecificationValues(
         array
      );
      if (!CreateJoin) {
        response(res, true, "cannot created join", Create);
      }
      response(res, true, "created data", Create)
    }
  } catch (error) {
    response(res, false, "cannot connect db", error);
  }
};

exports.read = async (req, res) => {
  //const access = JSON.parse(req.params.access);
  try {
    const Read = await data.findAll({include: [{ all: true, nested: false }] });
    if (!Read) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Read);
  } catch (error) {
    response(res, false, "cannot connect db", error);
  }
};

exports.readOne = async (req, res) => {
  const params = JSON.parse(req.params.param);
  try {
    const Read = await data.findOne({ where: params, include: [{ all: true, nested: false }, {model:employee, include:[{model:site},{model:location}, {model:department}]}] });
    if (!Read) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Read);
  } catch (error) {
    response(res, false, "cannot connect db", error);
  }
};

exports.update = async (req, res) => {
  const params = JSON.parse(req.params.param);
  const {
    tag_id,
    description,
    categoryId,
    brandId,
    modelId,
    serial_no,
    siteId,
    locationId,
    departmentId,
    lastOpname,
    statusId,
    instanceId,
    companyId,
    regionId,
    isActive,
    cost,
    purchaseId,
  } = req.body;
  try {
    const Update = await data.update(
      {
        tag_id,
        description,
        categoryId,
        brandId,
        modelId,
        serial_no,
        siteId,
        locationId,
        departmentId,
        lastOpname,
        statusId,
        instanceId,
        companyId,
        regionId,
        isActive,
        cost,
        purchaseId,
      },
      { where: params,logging:(...msg) => logging(msg, req.body.userId) }
    );
    if (!Update) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Update);
  } catch (error) {
    response(res, false, "cannot connect db", null);
  }
};

exports.delete = async (req, res) => {
  const params = JSON.parse(req.params.param);
  try {
    const Delete = await data.destroy({ where: params, logging:(...msg) => logging(msg, req.body.userId) });
    if (!Delete) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Delete);
  } catch (error) {
    response(res, false, "cannot connect db", null);
  }
};

exports.uploads = () => {
    const date = new Date();
    const monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    return multer({
      storage: multer.diskStorage({
        destination: function (req, res, cb) {
          cb(null, `D:/Data Zaki/v3/data/import/excel/asset`);
        },
        filename: function (req, file, cb) {
          cb(
            null,
            `asset-${date.getDate()}${
              monthNames[date.getMonth()]
            }${date.getFullYear()}_${date.getHours()}${
              (date.getMinutes() < 10 ? "0" : "") + date.getMinutes()
            }.xlsx`
          );
        },
      }),
    });
  };
  
  exports.import = (req, res) => {
    let row = [];
    try {
      readXlsxFile(req.body.path).then(async (rows) => {
        let header = rows[0];
        rows.shift();
        for (const row of rows) {
            const Category = await category.findCreateFind({
                where: { name: row[6] },logging:(...msg) => logging(msg, req.body.userId)
              });
              const CategoryStr = await JSON.stringify(Category);
              const CategoryObj = await JSON.parse(CategoryStr);
              const Brand = await brand.findCreateFind({
                where: { name: row[7], categoryId: CategoryObj[0].id },logging:(...msg) => logging(msg, req.body.userId)
              });
              const BrandStr = await JSON.stringify(Brand);
              const BrandObj = await JSON.parse(BrandStr);
              const Model = await mode.findCreateFind({
                where: { name: row[8], brandId: BrandObj[0].id },logging:(...msg) => logging(msg, req.body.userId)
              });
              const ModelStr = await JSON.stringify(Model);
              const ModelObj = await JSON.parse(ModelStr);
              const Site = await site.findCreateFind({where:{name:row[9]}})
              const SiteStr = await JSON.stringify(Site)
              const SiteObj = await JSON.parse(SiteStr)
              const Location = await location.findCreateFind({where:{name:row[10], siteId:SiteObj[0].id}})
              const LocationStr = await JSON.stringify(Location)
              const LocationObj = await JSON.parse(LocationStr)
              const Department = await department.findCreateFind({where:{name:row[11]}})
              const DepartmentStr = await JSON.stringify(Department)
              const DepartmentObj = await JSON.parse(DepartmentStr)
              const Instance = await instance.findCreateFind({where:{name:row[12]}})
              const InstanceStr = await JSON.stringify(Instance)
              const InstanceObj = await JSON.parse(InstanceStr)
              const Company = await company.findCreateFind({where:{name:row[13], instanceId:InstanceObj[0].id}})
              const CompanyStr = await JSON.stringify(Company)
              const CompanyObj = await JSON.parse(CompanyStr)
              const Region = await region.findCreateFind({where:{name:row[14], code:row[15], companyId:CompanyObj[0].id}})
              const RegionStr = await JSON.stringify(Region)
              const RegionObj = await JSON.parse(RegionStr)
              const Status = await Status.findCreateFind({where:{name:row[16], codeLr}})
              const StatusStr = await JSON.stringify(Status)
              const StatusObj = await JSON.parse(StatusStr)
              const Purchase = await JSON.findCreateFind({where:{order_number: row[16]}})
              const PurchaseStr = await JSON.stringify(Purchase)
              const PurchaseObj = await JSON.parse(PurchaseStr)

              const Asset = data.findCreateFind({
              where:{
                  tag_id: row[0],
                  description:row[1],
                  serial_no: row[2],
                  lastOpname: row[3],
                  isActive: row[4],
                  cost: row[5],
                  categoryId: CategoryObj[0].id,
                  brandsId: BrandObj[0].id,
                  modelId: ModelObj[0].id,
                  siteId: SiteObj[0].id,
                  locationId: LocationObj[0].id,
                  departmentId: DepartmentObj[0].id,
                  instanceId: InstanceObj[0].id,
                  companyId: CompanyObj[0].id,
                  regionId: RegionObj[0].id,
                  statusId : StatusObj[0].id,
                  purchaseId:  PurchaseObj[0].id  
              }
          })
          const AssetStr = JSON.stringify(Asset)
          const AssetObj = JSON.parse(AssetStr)
          for (let i = 0; i <= Count.count; i++) {
            if (row[17 + i]) {
              const Specification = await specification.findCreateFind({
                where: { name: header[17 + i], categoryId: CategoryObj[0].id },logging:(...msg) => logging(msg, req.body.userId)
              });
              const SpecificationStr = await JSON.stringify(Specification);
              const SpecificationObj = await JSON.parse(SpecificationStr);
              const Value = await specificationValue.findCreateFind({
                where: {
                  value: row[17 + i],
                  specificationId: SpecificationObj[0].id,
                },logging:(...msg) => logging(msg, req.body.userId)
              }, );
              const ValueStr = await JSON.stringify(Value);
              const ValueObj = await JSON.parse(ValueStr);
              const valueToAsset = await valueToAsset.findCreateFind({
                where: {
                  assetId: AssetObj[0].id,
                  specificationValueId: ValueObj[0].id,
                }, logging:(...msg) => logging(msg, req.body.userId),
              });
            }
          }
        }
        response(res, true, "imported", null);
      });
    } catch (error) {
      console.log(error);
    }
  };
  