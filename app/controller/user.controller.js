const {user} = require('../config/db.config')
const {response} = require('../middleware/response')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {logging} = require('../controller/log.controller')
exports.create = async (req, res) => {
    try{
        const {name, email, password, roleId} = req.body
        const hashedPassword = await bcrypt.hashSync(password, 10)
        const Create = await user.create(
            {name, email, hashedPassword, roleId},
            {logging:(...msg) => logging(msg, req.body.userId)}
        )
        if(Create){
            response(res, true, 'user created', Create)
        }else{
            response(res, false, 'cannot create user', null)
        }
    }catch(error){
        response(res, false, 'cannot connect db', null)
    }
}

exports.signup = async (req, res) => {
    const {name, email} = req.body
    try{
        const read = await user.findOne({email})
        if(read){
            return response(res, false, 'user exist', null)
        }
        const create = await user.create({name, email, password:bcrypt.hashSync(req.body.password, 10)})
        if(!create){
            return response(res, false, 'cannot create user', null)
        }
        return response(res, true, 'user created', null)
    }catch(error){
        return response(res, false, 'cannot connect db', null)
    }
}

exports.signin = async (req, res) => {
    try{
        const {email, password} = req.body
        const User = await user.findOne({where:{email: email}})
        if(!User){
            return response(res, false, 'user not found', null)
        }
        const validPassword = await bcrypt.compareSync(password, User.password)
        if(!validPassword){
            return response(res, false, 'password invalid', null) 
        }
        const token =  await jwt.sign({id:User.id, roleId:User.roleId}, process.env.SECRET, {
            expiresIn: 86400
        })
        return response(res, true, 'sign in', {token : token, id:User.id, roleId:User.roleId})
    }catch(error) {
        return response(res, false , 'cannot connect db', null)
    }
}

exports.read = async (req, res) => {
    const {condition} = JSON.parse(req.params.access)
    
    try{
        const Read = await user.findAll({attribute})
        if(!Read){
            return response(res, false, 'data not found')
        }
        return response(res, true, 'retrieved data', Read)
    }catch(error){
        return response(res, false , 'cannot connect db', null)
    }
}

exports.readOne = async (req, res) => {
    const params = JSON.parse(req.params.param)
    try{
        const Read = await user.findOne({
            where: params,
            include: [{all:true, nested: false}]
        })
        if(!Read){
            return response(res, false, 'data not found', null)
        }
        return response(res, true, 'data retrieved', Read)
    }catch(error){
        return response(res, false , 'cannot connect db', null)
    }
}

exports.update = async (req, res) => {
    const params = await JSON.parse(req.params.param)
    const {name, email, password} = req.body
    const hashedPassword = await bcrypt.hashSync(password, 10)
    try{
        const Update = await user.update({
            name, email, hashedPassword
        }, {where : params, logging:(...msg) => logging(msg, req.body.userId)})
        if(!Update){
            return response(res, false, 'data not found', null)
        }
        return response(res, true, 'data retrieved')
    }catch(error){
        return response(res, false , 'cannot connect db', null)
    }
}

exports.delete = async (req, res) => {
    const params = await JSON.parse(req.params.param)
    try{
        const Delete = await  user.destroy({where:params,
            logging:(...msg) => logging(msg, req.body.userId)})
        if(!Delete){
            return response(res, false, 'data not found', null)
        }
        return response(res, true, 'data deleted', Delete)
    }catch(error){
        return response(res, false, 'cannot connect db', null)
    }
}