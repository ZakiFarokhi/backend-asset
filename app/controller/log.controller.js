const db = require('../config/db.config')
exports.logging = async (data, userId) => {
    //console.log(data)
    try {
      const Log = db.log;
      const regex = /log/g;
      const match = data[0].match(regex);
      const value = data[1] ? JSON.stringify(data[1].bind) : null;
      const type = data[1] ? data[1].type : null;
      if (
        type !== "SELECT" &&
        type !== "RAW" &&
        type !== "SHOWINDEXES" &&
        type !== "DESCRIBE" &&
        type !== "FOREIGNKEYS" &&
        match === null
      ) {
        const log = Log.create({
          query: data[0],
          value: value,
          type: data[1] ? data[1].type : null,
          table: toString(data[1] ? data[1].model : null),
          userId:userId
        });
      }
    } catch (error) {
      //console.log(error);
    }
  };