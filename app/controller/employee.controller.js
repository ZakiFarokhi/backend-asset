const db = require("../config/db.config");
const data = db.employee;
const { response } = require("../middleware/response");
const { logging } = require("../controller/log.controller");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

exports.signup = async (req, res) => {
  const { id, name, email, siteId, locationId, departmentId } = req.body;
  try {
    const read = await data.findOne({ where: { email } });
    if (read) {
      return response(res, false, "user exist", read);
    }
    const create = await data.create({
      id,
      name,
      email,
      siteId,
      locationId,
      departmentId,
      password: bcrypt.hashSync(req.body.password, 10),
    });
    if (!create) {
      return response(res, false, "cannot create user", null);
    }
    return response(res, true, "user created", create);
  } catch (error) {
    return response(res, false, "cannot connect db", error);
  }
};

exports.signin = async (req, res) => {
  try {
    const { email, password } = req.body;
    const Data = await data.findOne({ where: { email: email } });
    if (!Data) {
      return response(res, false, "user not found", null);
    }
    const validPassword = await bcrypt.compareSync(password, Data.password);
    if (!validPassword) {
      return response(res, false, "password invalid", null);
    }
    //console.log(Data.id)
    const token = await jwt.sign({ id: Data.id }, process.env.SECRET, {
      expiresIn: 86400,
    });
    return response(res, true, "sign in", { token: token, id: Data.id });
  } catch (error) {
    return response(res, false, "cannot connect db", null);
  }
};

exports.create = async (req, res) => {
  try {
    const {
      name,
      title,
      phone,
      email,
      password,
      isActive,
      status,
      siteId,
      locationId,
      departmentId,
    } = req.body;
    const Create = await data.create(
      {
        name,
        title,
        phone,
        email,
        password,
        isActive,
        status,
        siteId,
        locationId,
        departmentId,
      },
      { logging: (...msg) => logging(msg, req.body.userId) }
    );
    if (Create) {
      response(res, true, "category created", Create);
    } else {
      response(res, false, "cannot create data", null);
    }
  } catch (error) {
    response(res, false, "cannot connect db", null);
  }
};

exports.read = async (req, res) => {
  try {
    const Read = await data.findAll({
      include: [{ all: true, nested: false }],
    });
    if (!Read) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Read);
  } catch (error) {
    response(res, false, "cannot connect db", error);
  }
};

exports.readOne = async (req, res) => {
  const params = JSON.parse(req.params.param);
  try {
    const Read = await data.findOne({
      where: params,
      include: [{ all: true, nested: false }],
    });
    if (!Read) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Read);
  } catch (error) {
    response(res, false, "cannot connect db", error);
  }
};

exports.update = async (req, res) => {
  const params = JSON.parse(req.params.param);
  const {
    name,
    title,
    phone,
    email,
    password,
    isActive,
    status,
    siteId,
    locationId,
    departmentId,
  } = req.body;
  try {
    const Update = await data.update(
      {
        name,
        title,
        phone,
        email,
        password,
        isActive,
        status,
        siteId,
        locationId,
        departmentId,
      },
      { where: params, logging: (...msg) => logging(msg, req.body.userId) }
    );
    if (!Update) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Update);
  } catch (error) {
    response(res, false, "cannot connect db", null);
  }
};

exports.delete = async (req, res) => {
  const params = JSON.parse(req.params.param);
  try {
    const Delete = await data.destroy({
      where: params,
      logging: (...msg) => logging(msg, req.body.userId),
    });
    if (!Delete) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Delete);
  } catch (error) {
    response(res, false, "cannot connect db", null);
  }
};
