const db = require("../config/db.config");
const data = db.event;
const { response } = require("../middleware/response");
const { logging } = require("../controller/log.controller");

exports.create = async (req, res) => {
  try {
    //console.log(data)
    const { name, description, date, document, assetId } = req.body;
    const Create = await data.create(
      { name, description, date, document, assetId },
      { logging: (...msg) => logging(msg, req.body.userId) }
    );
    if (Create) {
      response(res, true, "category created", Create);
    } else {
      response(res, false, "cannot create data", null);
    }
    //console.log(Create);
  } catch (er) {
    //console.log(er);
    response(res, false, "cannot connect db", er);
  }
};

exports.read = async (req, res) => {
  //const access = JSON.parse(req.params.access);
  try {
    const Read = await data.findAll({
      include: [{ all: true, nested: false }],
    });
    if (!Read) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Read);
  } catch (error) {
    response(res, false, "cannot connect db", error);
  }
};

exports.readOne = async (req, res) => {
  const params = JSON.parse(req.params.param);
  try {
    const Read = await data.findOne({
      where: params,
      include: [{ all: true, nested: false }],
    });
    if (!Read) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Read);
  } catch (er) {
    response(res, false, "cannot connect db", er);
  }
};

exports.update = async (req, res) => {
  const params = JSON.parse(req.params.param);
  const { name, description, data, document, assetId } = req.body;
  try {
    const Update = await data.update(
      { name, description, data, document, assetId },
      { where: params, logging: (...msg) => logging(msg, req.body.userId) }
    );
    if (!Update) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Update);
  } catch (error) {
    response(res, false, "cannot connect db", null);
  }
};

exports.delete = async (req, res) => {
  const params = JSON.parse(req.params.param);
  try {
    const Delete = await data.destroy({
      where: params,
      logging: (...msg) => logging(msg, req.body.userId),
    });
    if (!Delete) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Delete);
  } catch (error) {
    response(res, false, "cannot connect db", null);
  }
};
