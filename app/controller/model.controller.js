const db = require("../config/db.config");
const data = db.model;
const category = db.category;
const brand = db.brand;
const specification = db.specification;
const specificationValue = db.specificationValue;
const valueToModel = db.valueToModel;
const { response } = require("../middleware/response");
const multer = require("multer");
const readXlsxFile = require("read-excel-file/node");
const { logging } = require("../controller/log.controller");

exports.create = async (req, res) => {
  try {
    const { name, brandId } = req.body;
    const Create = await data.create(
      { name, brandId },
      { logging: (...msg) => logging(msg, req.body.userId) }
    );
    if (Create) {
      response(res, true, "category created", Create);
    } else {
      response(res, false, "cannot create data", null);
    }
  } catch (error) {
    response(res, false, "cannot connect db", null);
  }
};

exports.read = async (req, res) => {
//   //const access = JSON.parse(req.params.access);
  try {
    const Read = await data.findAll({ include: [{ all: true, nested: false }] });
    if (!Read) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Read);
  } catch (error) {
    response(res, false, "cannot connect db", error);
  }
};

exports.readOne = async (req, res) => {
  const params = JSON.parse(req.params.param);
  //console.log(params)
  try {
    const Read = await data.findOne({ where: params, include: [{ all: true, nested: false }] });
    if (!Read) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Read);
  } catch (error) {
    response(res, false, "cannot connect db", error);
  }
};

exports.update = async (req, res) => {
  const params = JSON.parse(req.params.param);
  const { name, brandId } = req.body;
  try {
    const Update = await data.update(
      { name, brandId },
      { where: params, logging: (...msg) => logging(msg, req.body.userId) }
    );
    if (!Update) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Update);
  } catch (error) {
    response(res, false, "cannot connect db", null);
  }
};

exports.delete = async (req, res) => {
  const params = JSON.parse(req.params.param);
  try {
    const Delete = await data.destroy({
      where: params,
      logging: (...msg) => logging(msg, req.body.userId),
    });
    if (!Delete) {
      response(res, false, "data not found", null);
    }
    response(res, true, "retrieved data", Delete);
  } catch (error) {
    response(res, false, "cannot connect db", null);
  }
};

exports.uploads = () => {
  const date = new Date();
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  return multer({
    storage: multer.diskStorage({
      destination: function (req, res, cb) {
        cb(null, `D:/Data Zaki/v3/data/import/excel/model`);
      },
      filename: function (req, file, cb) {
        cb(
          null,
          `model-${date.getDate()}${
            monthNames[date.getMonth()]
          }${date.getFullYear()}_${date.getHours()}${
            (date.getMinutes() < 10 ? "0" : "") + date.getMinutes()
          }.xlsx`
        );
      },
    }),
  });
};

exports.import = async (req, res) => {
  let row = [];
  //console.log(req.file.path);
  try {
    const readExcel = await readXlsxFile(req.body.path).then(async (rows) => {
      let header = rows[0];
      rows.shift();
      for (const row of rows) {
        const Category = await category.findCreateFind({
          where: { name: row[0] },
          logging: (...msg) => logging(msg, req.body.userId),
        });
        const CategoryStr = await JSON.stringify(Category);
        const CategoryObj = await JSON.parse(CategoryStr);
        const Brand = await brand.findCreateFind({
          where: { name: row[1], categoryId: CategoryObj[0].id },
          logging: (...msg) => logging(msg, req.body.userId),
        });
        const BrandStr = await JSON.stringify(Brand);
        const BrandObj = await JSON.parse(BrandStr);
        const Model = await data.findCreateFind({
          where: { name: row[2], brandId: BrandObj[0].id },
          logging: (...msg) => logging(msg, req.body.userId),
        });
        const ModelStr = await JSON.stringify(Model);
        const ModelObj = await JSON.parse(ModelStr);
        const Count = await specification.findAndCountAll();
        for (let i = 0; i <= Count.count; i++) {
          if (row[3 + i]) {
            const Specification = await specification.findCreateFind({
              where: { name: header[3 + i], categoryId: CategoryObj[0].id },
              logging: (...msg) => logging(msg, req.body.userId),
            });
            const SpecificationStr = await JSON.stringify(Specification);
            const SpecificationObj = await JSON.parse(SpecificationStr);
            const Value = await specificationValue.findCreateFind({
              where: {
                value: row[3 + i],
                specificationId: SpecificationObj[0].id,
              },
              logging: (...msg) => logging(msg, req.body.userId),
            });
            const ValueStr = await JSON.stringify(Value);
            const ValueObj = await JSON.parse(ValueStr);
            const ValueToModel = await valueToModel.findCreateFind({
              where: {
                modelId: ModelObj[0].id,
                specificationValueId: ValueObj[0].id,
              },
              logging: (...msg) => logging(msg, req.body.userId),
            });
          }
        }
      }
    });
    response(res, true, "submitted", valueToModel);
  } catch (error) {
    response(res, false, "cannot connect db", null);
  }
};
