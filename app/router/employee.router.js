module.exports = function(app){
    const controller = require('../controller/employee.controller')

    app.post('/auth/employee/signin/', controller.signin)

    app.post('/auth/employee/signup/', controller.signup)

    app.post('/employees/create/', controller.create)

    app.get('/employees/read/', controller.read)

    app.get('/employees/read/:param', controller.readOne)

    app.patch('/employees/update/:param', controller.update)

    app.delete('/employees/delete/:param', controller.delete)
}