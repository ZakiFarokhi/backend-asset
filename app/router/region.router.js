module.exports = function(app){
    const controller = require('../controller/region.controller')

    app.post('/regions/create/', controller.create)

    app.get('/regions/read/', controller.read)

    app.get('/regions/read/:param', controller.readOne)

    app.patch('/regions/update/:param', controller.update)

    app.delete('/regions/delete/:param', controller.delete)
}