module.exports = function(app){
    const controller = require('../controller/purchase.controller')

    app.post('/purchases/create/', controller.create)

    app.get('/purchases/read/', controller.read)

    app.get('/purchases/read/:param', controller.readOne)

    app.patch('/purchases/update/:param', controller.update)

    app.delete('/purchases/delete/:param', controller.delete)
}