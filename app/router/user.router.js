
module.exports = function(app) {
    const controller = require('../controller/user.controller');

    app.post('/auth/user/signup/', controller.signup);
   
    app.post('/auth/user/signin/', controller.signin);

    //see middleware.controller
    app.get('/users/read/' , controller.read)

    app.get('/users/read/:param', controller.readOne)

    app.post('/users/create/:param', controller.create)

    app.patch('/users/update/:param', controller.update)

    app.delete('/users/delete/:param', controller.delete)

}