module.exports = function(app){
    const controller = require('../controller/department.controller')

    app.post('/departments/create/', controller.create)

    app.get('/departments/read/', controller.read)

    app.get('/departments/read/:param', controller.readOne)

    app.patch('/departments/update/:param', controller.update)

    app.delete('/departments/delete/:param', controller.delete)
}