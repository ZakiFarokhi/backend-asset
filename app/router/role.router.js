module.exports = function(app){
    const controller = require('../controller/role.controller')

    app.post('/roles/create/', controller.create)

    app.get('/roles/read/', controller.read)

    app.get('/roles/read/:param', controller.readOne)

    app.patch('/roles/update/:param', controller.update)

    app.delete('/roles/delete/:param', controller.delete)
}