const { response } = require("../middleware/response");
const readExcelFile = require("read-excel-file/node");
module.exports = function (app) {
  const controller = require("../controller/model.controller");

  app.post("/models/create/", controller.create);

  app.get("/models/read/", controller.read);

  app.get("/models/read/:param", controller.readOne);

  app.patch("/models/update/:param", controller.update);

  app.delete("/models/delete/:param", controller.delete);

  app.post(
    "/models/import/",
    controller.uploads().single("excel"),
    (req, res, next) => {
      //console.log(req.file.path);
      readExcelFile(req.file.path).then(async (rows) => {
        const header = await rows.shift();
        let column = [];
        let dataSource = [];
        for (let i = 0; i < header.length; i++) {
          column.push({
            key: header[i].toLowerCase(),
            dataIndex: header[i],
            title: header[i].toLowerCase(),
          });
        }
        for (const row of rows) {
          let datas = { key: rows.indexOf(row) };
          let assign = [];
          let val;
          for (let i = 0; i < header.length; i++) {
            colom = [header[i]];
            val = row[i];
            assign = Object.assign(datas, { [colom]: val });
          }
          dataSource.push(assign);
        }
        const data = {
          data: { column: column, dataSource: dataSource },
          path: req.file.path,
        };
        response(res, true, "imported", data);
      });
    }
  );

  app.post("/models/import/submit", controller.import);
};
