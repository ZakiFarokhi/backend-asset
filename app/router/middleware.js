const auth = require('../middleware/jsonwebtoken');
const access = require('../middleware/control.access')

module.exports = function (app) {

    app.all('/v1/:table/:action/',auth, access.bridges)
    
    app.all('/v1/:table/:action/:param',auth, access.bridges)
}