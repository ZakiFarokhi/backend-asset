module.exports = function(app){
    const controller = require('../controller/category.controller')

    app.post('/categories/create/', controller.create)

    app.get('/categories/read/', controller.read)

    app.get('/categories/read/:param', controller.readOne)

    app.patch('/categories/update/:param', controller.update)

    app.delete('/categories/delete/:param', controller.delete)
}