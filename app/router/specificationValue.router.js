module.exports = function(app){
    const controller = require('../controller/specificationValue.controller')

    app.post('/specificationValues/create/', controller.create)

    app.get('/specificationValues/read/', controller.read)

    app.get('/specificationValues/read/:param', controller.readOne)

    app.patch('/specificationValues/update/:param', controller.update)

    app.delete('/specificationValues/delete/:param', controller.delete)
}