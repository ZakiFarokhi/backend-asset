const auth = require('../middleware/jsonwebtoken')
const access = require('../middleware/control.access')
module.exports = function(app){
    const controller = require('../controller/brand.controller')

    app.post('/brands/create/', controller.create)

    app.get('/brands/read/', auth, access.bridges, controller.read)

    app.get('/brands/read/:param', controller.readOne)

    app.patch('/brands/update/:param', controller.update)

    app.delete('/brands/delete/:param', controller.delete)
}