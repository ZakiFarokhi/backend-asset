module.exports = function(app){
    const controller = require('../controller/specification.controller')

    app.post('/specifications/create/', controller.create)

    app.get('/specifications/read/', controller.read)

    app.get('/specifications/read/:param', controller.readOne)

    app.patch('/specifications/update/:param', controller.update)

    app.delete('/specifications/delete/:param', controller.delete)
}