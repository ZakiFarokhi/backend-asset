module.exports = function(app){
    const controller = require('../controller/company.controller')

    app.post('/companies/create/', controller.create)

    app.get('/companies/read/', controller.read)

    app.get('/companies/read/:param', controller.readOne)

    app.patch('/companies/update/:param', controller.update)

    app.delete('/companies/delete/:param', controller.delete)
}