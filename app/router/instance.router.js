module.exports = function(app) {
    const controller = require('../controller/instance.controller')

    app.get('/instances/read/', controller.read)

    app.get('/instances/read/:param', controller.readOne)

    app.post('/instances/create/', controller.create)

    app.patch('/instances/update/:param', controller.update)

    app.delete('/instances/delete/:param', controller.delete)


}