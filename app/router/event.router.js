const {response} = require('../middleware/response')
const multer = require('multer')
module.exports = function (app) {
    const controller = require('../controller/event.controller')
    var date = new Date()
    const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    const storage = multer.diskStorage({
        destination: function (req, res, cb) {
            cb(null, `D:/Data Zaki/ITAsset/assets/upload/${req.params.event}`)
        },
        filename: function (req, file, cb) {
            cb(null, `${req.params.asset}-${date.getDate()}${monthNames[date.getMonth()]}${date.getFullYear()}_${date.getHours()}${(date.getMinutes()<10?'0':'') + date.getMinutes()}.png`)
        }
    })
    var upload = multer({ storage: storage })

    app.post('/events/upload/:event/:asset', upload.single('photo'), (req, res, next) => {
        return response(res, true, 'uploaded', req.file.path)
    })

    app.get('/events/read/', controller.read)

    app.get('/events/read/:param', controller.readOne)

    app.post('/events/create/', controller.create)

    app.patch('/events/update/:param', controller.update)

    app.delete('/events/delete/:param', controller.delete)

}