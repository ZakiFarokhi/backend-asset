module.exports = function(app){
    const controller = require('../controller/status.controller')

    app.post('/status/create/', controller.create)

    app.get('/status/read/', controller.read)

    app.get('/status/read/:param', controller.readOne)

    app.patch('/status/update/:param', controller.update)

    app.delete('/status/delete/:param', controller.delete)
}