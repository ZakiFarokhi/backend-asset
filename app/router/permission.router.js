module.exports = function(app){
    const controller = require('../controller/permission.controller')

    app.post('/permissions/create/', controller.create)

    app.get('/permissions/read/', controller.read)

    app.get('/permissions/read/:param', controller.readOne)

    app.patch('/permissions/update/:param', controller.update)

    app.delete('/permissions/delete/:param', controller.delete)
}