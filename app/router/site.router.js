module.exports = function(app){
    const controller = require('../controller/site.controller')

    app.post('/sites/create/', controller.create)

    app.get('/sites/read/', controller.read)

    app.get('/sites/read/:param', controller.readOne)

    app.patch('/sites/update/:param', controller.update)

    app.delete('/sites/delete/:param', controller.delete)
}