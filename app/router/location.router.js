
module.exports = function (app) {
    const controller = require("../controller/location.controller");
  
    app.get("/locations/read/", controller.read);
  
    app.get("/locations/read/:param", controller.readOne);
  
    app.post("/locations/create/", controller.create);
  
    app.patch("/locations/update/:param", controller.update);
  
    app.delete("/locations/delete/:param", controller.delete);
  };
  