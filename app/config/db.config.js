const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  process.env.DATABASE,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT, //configure db Microsoft SQL Server
    operatorsAliases: "0",
    timezone: "+07:00", //indonesia GMT +7
    logging:true,
    //logging: (...msg) => logging(msg), //console.log('tru',msg[0],' bind', JSON.stringify(msg[1].bind)),
    dialectOptions: {
      encrypt: true,
    },
    pool: {
      max: parseInt(process.env.DB_POOL_MAX),
      min: parseInt(process.env.DB_POOL_MIN),
      acquire: parseInt(process.env.DB_POOL_ACQUIRE),
      idle: parseInt(process.env.DB_POOL_IDLE),
    },
  }
);

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../model/user.model")(sequelize, Sequelize);
db.employee = require("../model/employee.model")(sequelize, Sequelize);
db.role = require("../model/role.model")(sequelize, Sequelize);
db.log = require("../model/log.model")(sequelize, Sequelize);
db.permission = require("../model/permission.model")(sequelize, Sequelize);

db.asset = require("../model/asset.model")(sequelize, Sequelize);
db.brand = require("../model/brand.model")(sequelize, Sequelize);
db.category = require("../model/category.model")(sequelize, Sequelize);
db.company = require("../model/company.model")(sequelize, Sequelize);
db.department = require("../model/department.model")(sequelize, Sequelize);
db.event = require("../model/event.model")(sequelize, Sequelize);
db.instance = require("../model/instance.model")(sequelize, Sequelize);
db.location = require("../model/location.model")(sequelize, Sequelize);
db.message = require("../model/message.model")(sequelize, Sequelize);
db.model = require("../model/model.model")(sequelize, Sequelize);
db.purchase = require("../model/purchase.model")(sequelize, Sequelize);
db.region = require("../model/region.model")(sequelize, Sequelize);
db.site = require("../model/site.model")(sequelize, Sequelize);
db.specification = require("../model/specification.model")(
  sequelize,
  Sequelize
);
db.specificationValue = require("../model/specificationValue.model")(
  sequelize,
  Sequelize
);
db.status = require("../model/status.model")(sequelize, Sequelize);

//join Table
db.valueToModel = require("../model/valueToModel.model")(sequelize, Sequelize);
db.valueToAsset = require("../model/valueToAsset")(sequelize, Sequelize);

//user association
db.role.hasMany(db.user);
db.user.belongsTo(db.role);
db.role.hasMany(db.permission);
db.permission.belongsTo(db.role);
db.user.hasMany(db.log)
db.log.belongsTo(db.user)

//employee association
db.employee.hasMany(db.asset)
db.asset.belongsTo(db.employee)
db.site.hasMany(db.employee)
db.employee.belongsTo(db.site)
db.location.hasMany(db.employee)
db.employee.belongsTo(db.location)
db.department.hasMany(db.employee)
db.employee.belongsTo(db.department)

//asset association
db.category.hasMany(db.asset);
db.asset.belongsTo(db.category);
db.brand.hasMany(db.asset);
db.asset.belongsTo(db.brand);
db.model.hasMany(db.asset);
db.asset.belongsTo(db.model);
db.site.hasMany(db.asset);
db.asset.belongsTo(db.site);
db.location.hasMany(db.asset);
db.asset.belongsTo(db.location);
db.department.hasMany(db.asset);
db.asset.belongsTo(db.department);
db.instance.hasMany(db.asset);
db.asset.belongsTo(db.instance);
db.company.hasMany(db.asset);
db.asset.belongsTo(db.company);
db.region.hasMany(db.asset);
db.asset.belongsTo(db.region);
db.status.hasMany(db.asset);
db.asset.belongsTo(db.status);
db.purchase.hasMany(db.asset);
db.asset.belongsTo(db.purchase);
db.asset.hasMany(db.event);
db.event.belongsTo(db.asset);

db.site.hasMany(db.location);
db.location.belongsTo(db.site);

db.category.hasMany(db.brand);
db.brand.belongsTo(db.category);
db.brand.hasMany(db.model);
db.model.belongsTo(db.brand);

db.instance.hasMany(db.company);
db.company.belongsTo(db.instance);
db.company.hasMany(db.region);
db.region.belongsTo(db.company);

db.specification.hasMany(db.specificationValue);
db.specificationValue.belongsTo(db.specification);

db.category.hasMany(db.specification);
db.specification.belongsTo(db.category);

db.model.belongsToMany(db.specificationValue, {
  through: db.valueToModel,
  foreignKey: "modelId",
});
db.specificationValue.belongsToMany(db.model, {
  through: db.valueToModel,
  foreignKey: "specificationValueId",
});

db.asset.belongsToMany(db.specificationValue, {
  through: db.valueToAsset,
  foreignKey: "assetId",
});
db.specificationValue.belongsToMany(db.asset, {
  through: db.valueToAsset,
  foreignKey: "specificationValueId",
});

module.exports = db;

const logging = async (data) => {
  //console.log(data)
  try {
    const Log = db.log;
    const regex = /log/g;
    const match = data[0].match(regex);
    const value = data[1] ? JSON.stringify(data[1].bind) : null;
    const type = data[1] ? data[1].type : null;
    if (
      type !== "SELECT" &&
      type !== "RAW" &&
      type !== "SHOWINDEXES" &&
      type !== "DESCRIBE" &&
      type !== "FOREIGNKEYS" &&
      match === null
    ) {
      const log = Log.create({
        query: data[0],
        value: value,
        type: data[1] ? data[1].type : null,
        table: toString(data[1] ? data[1].model : null),
      });
    }
  } catch (error) {
    //console.log(error);
  }
};
